package main

import (
	"log"
	"time"

	"gitlab.com/Fallion/shinkei-suijaku/cmd"
)

func main() {
	start := time.Now()
	inputFilePath := "/input.txt"

	links, err := cmd.LoadInputFile(inputFilePath)

	if err != nil {
		log.Fatal(err)
	}

	deduped := cmd.DeduplicateLinks(links)
	log.Printf("Loaded %d links", len(deduped))
	if len(deduped) != len(links) {
		log.Printf("%d out of %d were duplicate links and were discarded", len(links)-len(deduped), len(links))
	}

	err = cmd.RunApp(deduped, "/output.csv")

	if err != nil {
		log.Fatal(err)
	}

	elapsed := time.Since(start)
	log.Printf("Execution took %dus", elapsed)
}
