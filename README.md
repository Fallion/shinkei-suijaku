# Shinkei Suijaku (Pexeso)

This app serves as a solution to the homework provided by Pex. Task definition is provided [HERE](https://gist.github.com/ehmo/e736c827ca73d84581d812b3a27bb132).

## Usage

### Requirements

To run this app the following is required:

- Golang 1.13
- make (optional)
- A file containing a list of links (separated by a new line)

### Running the app

Please put an `input.txt` file with newline separated links in the same folder as the app was run.

For dev usage:

`go run main.go`

For production use:

Build using : `go build -o build/pexeso main.go` and then run `./build/pexeso`.

Output is put into `output.csv` in the same folder as the app was run.

## Testing

To run tests please run: `make test`

## Stretch goals

I wanted to add flags for selecting files into this app. However due to time constraints I have decided to skip this.
