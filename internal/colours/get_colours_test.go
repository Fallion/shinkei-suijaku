package colours

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetColoursRed(t *testing.T) {
	file, err := os.Open("testdata/red.png")

	defer file.Close()

	profile, err := GetColours(file)

	assert.NoError(t, err)
	assert.Equal(t, []string{"#FF0000"}, profile)
}

func TestGetColoursGreen(t *testing.T) {
	file, err := os.Open("testdata/green.png")

	defer file.Close()

	profile, err := GetColours(file)

	// This run is expected to return nothing as the library filters out green masks
	assert.Error(t, err)
	assert.Equal(t, []string(nil), profile)
}

func TestGetColoursBlue(t *testing.T) {
	file, err := os.Open("testdata/blue.png")

	defer file.Close()

	profile, err := GetColours(file)

	assert.NoError(t, err)
	assert.Equal(t, []string{"#0000FF"}, profile)
}

func TestGetColoursTwoColour(t *testing.T) {
	file, err := os.Open("testdata/two-colour.png")

	defer file.Close()

	profile, err := GetColours(file)

	assert.NoError(t, err)
	// There is a third colour present in the output which is a blend of the two colours.
	// It changes based on where the centroid is placed.
	assert.Contains(t, profile, "#A86899")
	assert.Contains(t, profile, "#888888")
}
