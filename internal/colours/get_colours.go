package colours

import (
	"fmt"
	"image"

	// Support for png
	_ "image/png"
	// Support for jpeg
	_ "image/jpeg"
	"os"

	"github.com/EdlinOrg/prominentcolor"
)

// GetColours reads the image file and returns back the prominent colours of that file.
func GetColours(file *os.File) ([]string, error) {
	image, _, err := image.Decode(file)

	if err != nil {
		return nil, fmt.Errorf("failed to decode %s: %s", file.Name(), err)
	}

	colours, err := prominentcolor.KmeansWithArgs(prominentcolor.ArgumentNoCropping, image)

	if err != nil {
		return nil, err
	}

	colourHashes := make([]string, len(colours))

	for index, colour := range colours {
		colourHashes[index] = fmt.Sprintf("#" + colour.AsString())
	}

	return colourHashes, nil
}
