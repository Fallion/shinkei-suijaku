package csv

import (
	"io/ioutil"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSaveEmptyFilename(t *testing.T) {
	doc := &Document{}

	err := doc.Save("")

	assert.Error(t, err)
}

func TestEmptyDataSet(t *testing.T) {
	doc := &Document{Name: "test.csv"}

	err := doc.Save("/")

	assert.Error(t, err)
}

func TestFormatDocument(t *testing.T) {
	doc := &Document{Name: "test"}

	doc.AddData("1", "2", "3", "4")
	doc.AddData("test", "2", "5", "6")

	formattedData := formatDocument(doc.Data)

	referenceFile, err := ioutil.ReadFile("./testdata/reference.csv")

	assert.NoError(t, err)

	assert.Equal(t, string(referenceFile), formattedData)
}
