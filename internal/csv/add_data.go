package csv

import "fmt"

// AddData adds given strings to a new line in the CSV file. Maximum input length is 4.
func (d *Document) AddData(data ...string) error {
	if len(data) > 4 {
		return fmt.Errorf("failed to add data, max length is 4, given length is %d", len(data))
	}

	d.Data = append(d.Data, data)

	return nil
}
