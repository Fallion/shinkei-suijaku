package csv

// Document is a struct containing all the information required to create a CSV document
type Document struct {
	Name string
	Data [][]string
}
