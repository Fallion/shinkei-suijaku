package csv

import (
	"errors"
	"fmt"
	"io/ioutil"
	"strings"
)

// Save will save the file to a given path with the Filename provided in the Document struct. Path should not contain trailing slash and is relative to current directory.
func (d *Document) Save(path string) error {
	if d.Name == "" {
		return errors.New("missing filename")
	}

	if len(d.Data) == 0 {
		return errors.New("empty dataset")
	}

	formattedData := formatDocument(d.Data)

	// dot is added to make sure files save locally as opposed to going to the filesystem root
	fullPath := fmt.Sprintf(".%s/%s.csv", path, d.Name)

	err := ioutil.WriteFile(fullPath, []byte(formattedData), 0644)

	if err != nil {
		return err
	}

	return nil
}

// formatDocument prepares a string in the CSV compatible format
func formatDocument(data [][]string) string {

	builder := strings.Builder{}

	for _, line := range data {
		builder.WriteString(formatLine(line))
		// Terminate line
		builder.WriteString("\n")
	}

	return builder.String()
}

func formatLine(data []string) string {
	builder := strings.Builder{}
	lineLength := len(data)

	for index, item := range data {
		builder.WriteString(item)
		// CSV separates columns via a comma
		if index+1 != lineLength {
			builder.WriteString(",")
		}
	}

	return builder.String()
}
