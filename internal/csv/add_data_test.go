package csv

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAddData(t *testing.T) {
	doc := &Document{}

	err := doc.AddData("1", "2", "3", "4")

	assert.NoError(t, err)
	assert.Equal(t, []string{"1", "2", "3", "4"}, doc.Data[0])
}

func TestAddDataLongArray(t *testing.T) {
	doc := &Document{}

	err := doc.AddData("1", "2", "3", "4", "5")

	assert.Error(t, err)
}
