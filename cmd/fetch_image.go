package cmd

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"time"
)

// Download contains the url and path to temporary file
type Download struct {
	URL  string
	Path string
}

func fetchImage(url string) (Download, error) {
	client := http.Client{
		Timeout: time.Second * 10,
	}

	response, err := client.Get(url)

	if err != nil {
		return Download{}, err
	}

	if response.StatusCode != 200 {
		return Download{}, fmt.Errorf("%v returned %v code with error: %v", url, response.StatusCode, response.Status)
	}

	defer response.Body.Close()

	filepath := buildSavePath(url)

	err = saveFile(filepath, response.Body)

	if err != nil {
		return Download{}, err
	}

	return Download{
		URL:  url,
		Path: filepath,
	}, nil
}

func saveFile(path string, body io.Reader) error {
	_, err := ioutil.ReadDir("./tmp")

	if err != nil {
		err = os.MkdirAll("./tmp", os.ModePerm)

		if err != nil {
			return err
		}
	}

	file, err := os.Create(path)

	if err != nil {
		return err
	}
	defer file.Close()

	_, err = io.Copy(file, body)
	if err != nil {
		return err
	}

	return nil
}
