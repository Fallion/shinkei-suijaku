package cmd

// DeduplicateLinks removes extra links that are would cause pointless runs
func DeduplicateLinks(links []string) []string {
	var deduped []string
	itemMap := make(map[string]bool)

	for _, link := range links {
		if itemMap[link] {
			continue
		}
		itemMap[link] = true
		deduped = append(deduped, link)
	}

	return deduped
}
