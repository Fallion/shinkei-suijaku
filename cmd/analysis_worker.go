package cmd

import (
	"log"
	"os"
	"sync"

	"gitlab.com/Fallion/shinkei-suijaku/internal/colours"
	"gitlab.com/Fallion/shinkei-suijaku/internal/csv"
)

func worker(
	jobChan <-chan Download,
	appErrs *AppErrors,
	doc *csv.Document,
	waitGroup *sync.WaitGroup,
) error {
	defer waitGroup.Done()

	// Find prominent colours
	for download := range jobChan {
		// Skip empty string paths
		if download.Path == "" {
			continue
		}

		log.Println("starting analysis ", download.Path)
		file, err := os.Open(download.Path)

		if err != nil {
			appErrs.analysisErrs = append(appErrs.analysisErrs, err)
			continue
		}
		prominentColours, err := colours.GetColours(file)

		if err != nil {
			appErrs.analysisErrs = append(appErrs.analysisErrs, err)
			continue
		}

		arguments := prominentColours
		arguments = append([]string{download.URL}, arguments...)

		doc.AddData(arguments...)
		log.Println("Finished analysis ", download.Path)
	}

	return nil
}
