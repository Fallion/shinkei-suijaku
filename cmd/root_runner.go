package cmd

import (
	"fmt"
	"log"
	"runtime"
	"sync"

	"gitlab.com/Fallion/shinkei-suijaku/internal/csv"
)

// AppErrors hold all the errors that happened while running this app.
type AppErrors struct {
	fileErrs     []error
	analysisErrs []error
}

// RunApp run the image downloader and image analyser based on given links.
func RunApp(links []string, outputFilename string) error {
	// Assume 2 threads per CPU
	maxWorkers := runtime.NumCPU() * 2

	appErrs := &AppErrors{}

	filePathChan := make(chan Download, len(links))

	// Fetch inputs
	go imageChannel(links, filePathChan, appErrs)

	doc := &csv.Document{Name: "output"}

	var wg sync.WaitGroup

	for i := 0; i < maxWorkers; i++ {
		wg.Add(1)
		go worker(filePathChan, appErrs, doc, &wg)
	}

	wg.Wait()

	// Save csv
	err := doc.Save("")

	if err != nil {
		return err
	}

	fmt.Print("\n\n")
	log.Printf("Checked %d of %d links, %d failed to download, %d failed to analyse", len(doc.Data), len(links), len(appErrs.fileErrs), len(appErrs.analysisErrs))

	for index, fetchErr := range appErrs.fileErrs {
		if index == 0 {
			log.Println("Following urls had errors:")
		}
		log.Println(fetchErr)
	}

	for index, imageErr := range appErrs.analysisErrs {
		if index == 0 {
			log.Println("Following images had errors:")
		}
		log.Println(imageErr)
	}

	// Log output results
	fmt.Print("\n\n")
	log.Println("Sucessfully finished, file saved to output.csv")

	return nil
}
