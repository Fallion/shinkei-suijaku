package cmd

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLoadInputFile(t *testing.T) {
	links, err := LoadInputFile("/testdata/input")

	assert.NoError(t, err)

	expected := []string{
		"http://test.com/",
		"http://moretest.com/",
	}

	assert.Equal(t, expected, links)
}
