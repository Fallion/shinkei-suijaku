package cmd

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFetchImage(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {

		testFile, err := ioutil.ReadFile("./testdata/blue.png")

		assert.NoError(t, err)

		_, err = rw.Write([]byte(string(testFile)))

		assert.NoError(t, err)
	}))

	defer server.Close()

	download, err := fetchImage(fmt.Sprintf("%s/blue.png", server.URL))

	assert.NoError(t, err)
	assert.Contains(t, download.Path, "blue.png")
	assert.Equal(t, fmt.Sprintf("%s/blue.png", server.URL), download.URL)
}
