package cmd

import (
	"log"
	"sync"
)

func imageChannel(links []string, pathChannel chan Download, appErrs *AppErrors) {
	var wg sync.WaitGroup

	for index, link := range links {
		wg.Add(1)
		go func(link string, index int) {
			defer wg.Done()

			log.Println("Downloading: ", link)
			filePath, err := fetchImage(link)

			log.Printf("Downloaded %d of %d", index+1, len(links))

			pathChannel <- filePath

			if err != nil {
				appErrs.fileErrs = append(appErrs.fileErrs, err)
			}
		}(link, index)
	}

	wg.Wait()

	close(pathChannel)
}
