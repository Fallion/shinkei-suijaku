package cmd

import (
	"crypto/md5"
	"fmt"
	"path"
)

func buildSavePath(url string) string {
	// Get filename from the last part of the URL.
	filename := path.Base(url)
	// md5 hash is added to prevent edge cases where files have same name but different urls
	filepath := fmt.Sprintf("./tmp/%x-%s", md5.Sum([]byte(url)), filename)

	return filepath
}
