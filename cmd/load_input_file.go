package cmd

import "io/ioutil"

import "fmt"

import "strings"

// LoadInputFile loads the input file from path relative to current. It parses the links provided.
func LoadInputFile(path string) ([]string, error) {
	relativePath := fmt.Sprintf(".%s", path)
	file, err := ioutil.ReadFile(relativePath)

	if err != nil {
		return nil, err
	}

	links := strings.Split(string(file), "\n")

	// check for any empty strings after the split
	var filteredLinks []string

	for _, link := range links {
		if link != "" {
			filteredLinks = append(filteredLinks, link)
		}
	}

	return filteredLinks, nil
}
