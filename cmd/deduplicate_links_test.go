package cmd

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDeduplicateLinks(t *testing.T) {
	test := []string{
		"http://test.com/someimage.png",
		"http://test.com/someotherimage.png",
		"http://test.com/someimage.png",
	}

	deduped := DeduplicateLinks(test)

	assert.Equal(t, 2, len(deduped))
	assert.Contains(t, deduped, "http://test.com/someimage.png")
	assert.Contains(t, deduped, "http://test.com/someotherimage.png")
}
