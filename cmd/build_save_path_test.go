package cmd

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBuildSavePath(t *testing.T) {
	tests := map[string]string{
		"./tmp/eee19e63a020e5449afd483738da9900-test.png":         "http://someurl.com/test.png",
		"./tmp/dc72ef942c0622546125e230fb750921-w5q6gldnvcuy.jpg": "https://i.redd.it/w5q6gldnvcuy.jpg",
		"./tmp/b1c0767c2c43b4130471456b9bc76a2f-hi.png":           "https://someurl.com/nesting/deep/nesting/hi.png",
	}

	for expected, url := range tests {
		path := buildSavePath(url)

		assert.Equal(t, expected, path)
	}
}
